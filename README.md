# fastapi-example
## Entorno de producción

Desde afuera del contenedor, debemos construir el contenedor:
```bash
make build
```

Para correr el contenedor:
```bash
make run
```

Para probarlo corremos la instrucción:
```bach
curl localhost:80
```

Para abrir la documentación vemos la página:
```bash
http://localhost/docs
```

## Entorno de desarrollo

Abrimos el contenedor desde el vscode y trabajamos como de costumbre:
- Nos cambiamos a la carpeta `app` y corremos las instrucciones desde ahí. Por ejemplo:

```bash
make mutants
```

Para probar que todo está funcionando, corremos la instrucción:
```bach
curl localhost:80
```
