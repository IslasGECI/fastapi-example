
all: build run

build:
	docker build --tag islasgeci/fastapi_example .

run:
	docker run --rm --detach --name fastapi_example --publish 80:80 islasgeci/fastapi_example
