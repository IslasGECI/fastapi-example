import numpy as np
from app.main import init_bd, Item, PATH_DATABASE


def test_init_bd():
    data_bases = init_bd()
    column_names_obtained = data_bases.columns
    column_names_expected = ["name", "description", "price", "tax"]
    assert np.all(column_names_expected == column_names_obtained)
    expected_first_row = ["Nepo", "Humano", 0, 5]
    obtained_first_row = data_bases.values.tolist()[0]
    assert expected_first_row == obtained_first_row


def test_item():
    item_with_optional = Item(**{"name": "Nepo", "price": 0})
    assert item_with_optional.description is None
    assert item_with_optional.tax is None


def test_variable_path_database():
    assert PATH_DATABASE == "/tmp/data_base.csv"
