from fastapi.testclient import TestClient

from app.main import app

client = TestClient(app)


def test_read_root():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Hello World from GECI - Ciencia de Datos!"}


def test_make_add():
    response = client.get("/make_add")
    assert response.status_code == 200
    assert response.json() == {"add": 10}


def test_create_item():
    response = client.post(
        "/items/",
        headers={"accept": "application/json"},
        json={"name": "Nepo", "description": "Analista", "price": 2, "tax": 1},
    )
    assert response.status_code == 201
    assert response.json() == {"name": "Nepo", "description": "Analista", "price": 2, "tax": 1}


def test_get_all_bd():
    response = client.get("/all_bd")
    assert response.status_code == 200
    assert response.json() == {
        "id": {"0": 0, "1": 1},
        "name": {"0": "Nepo", "1": "Nepo"},
        "description": {"0": "Humano", "1": "Analista"},
        "price": {"0": 0.0, "1": 2.0},
        "tax": {"0": 5.0, "1": 1.0},
    }
